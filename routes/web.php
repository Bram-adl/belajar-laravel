<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home
// Route::get('/', 'HomeController@index');

// Register
// Route::get('/register', 'AuthController@register')->name('register');

// Welcome
// Route::post('/welcome', 'AuthController@welcome')->name('welcome');

Route::get('/', function () {
  return view('tables.index');
})->name('index');

Route::get('/data-tables', function () {
  return view('tables.data');
})->name('tables.data');

Route::resource('pertanyaan', 'PertanyaanController');