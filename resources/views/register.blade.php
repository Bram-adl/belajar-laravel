<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  <h1>Buat Account Baru!</h1>

  <h2>Sign Up Form</h2>

  <form action="{{ route('welcome') }}" method="POST" autocomplete="off">
    {{ csrf_field() }}
  
    <p>First Name:</p>
    <input type="text" name="firstName">

    <p>Last Name:</p>
    <input type="text" name="lastName">

    <p>Gender:</p>
    <input type="radio" name="gender" value="Male">Male <br>
    <input type="radio" name="gender" value="Female">Female <br>
    <input type="radio" name="gender" value="Other">Other

    <p>Nationality:</p>
    <select name="nationality">
      <option value="Indonesia">Indonesia</option>
      <option value="Singapore">Singapore</option>
      <option value="Malaysia">Malaysia</option>
      <option value="Australia">Australia</option>
    </select>

    <p>Language Spoken:</p>
    <input type="checkbox" name="language" value="Bahasa Indonesia">Bahasa Indonesia <br>
    <input type="checkbox" name="language" value="English">English <br>
    <input type="checkbox" name="language" value="Other">Other

    <p>Bio:</p>
    <textarea name="bio" cols="30" rows="10"></textarea> <br>

    <button type="submit">Sign Up</button>
  </form>


</body>
</html>