@extends('layouts.master')

@push('styles')
<link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">

<style>
  form {
    display: inline-block;
  }
  i.fas {
    cursor: pointer;
  }
  .my-btn {
    background: none;
    border: none;
    outline: none;
  }
</style>
@endpush

@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Tabel Pertanyaan</h3>
    <a href="{{ action('PertanyaanController@create') }}" class="btn btn-sm btn-primary float-right">
      <i class="fas fa-plus mr-1"></i>
      Tambah pertanyaan
    </a>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <table id="example1" class="table table-bordered">
      <thead>                  
        <tr>
          <th style="width: 10px">No</th>
          <th>Judul</th>
          <th>Pertanyaan</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        @foreach ($pertanyaan as $key=>$value)
        <tr>
          <td>{{ $key + 1 }}</td>
          <td>{{ $value->judul }}</td>
          <td>{{ $value->isi }}</td>
          <td>
            <div class="btn-group">
              <a href="{{ action('PertanyaanController@show', $value->id) }}" class="badge badge-primary"><i class="fas fa-eye"></i></a>
              <a href="{{ action('PertanyaanController@edit', $value->id) }}" class="badge badge-success mx-2"><i class="fas fa-edit"></i></a>
              <a href="#" data-toggle="modal" data-target="#exampleModal" class="badge badge-danger"><i class="fas fa-trash"></i></a>
            </div>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <!-- /.card-body -->

  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Hapus Pertanyaan ?</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-info" data-dismiss="modal">Tutup</button>
          <form action="{{ action('PertanyaanController@destroy', $value->id) }}" method="post">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger">Hapus</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@if (session('store'))
  @push('scripts')
  <script>
    $(function () {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
      })    

      Toast.fire({
        icon: 'success',
        title: 'Pertanyaan berhasil dibuat'
      })
    })
  </script>
  @endpush
@endif

@if (session('update'))
  @push('scripts')
  <script>
    $(function () {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
      })    

      Toast.fire({
        icon: 'success',
        title: 'Pertanyaan berhasil diperbaharui'
      })
    })
  </script>
  @endpush
@endif

@if (session('destroy'))
  @push('scripts')
  <script>
    $(function () {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
      })    

      Toast.fire({
        icon: 'success',
        title: 'Pertanyaan berhasil dihapus'
      })
    })
  </script>
  @endpush
@endif

@push('scripts')
<script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>

<script>
  $(function () {
    $("#example1").DataTable();
  });
  
  let nav = document.querySelector('nav ul.nav')
  let navTreeview = nav.querySelectorAll('.has-treeview')

  navTreeview[0].classList.add('menu-open')
  navTreeview[0].children[0].classList.add('active')
  navTreeview[0].children[1].children[1].children[0].classList.add('active')
</script>
@endpush