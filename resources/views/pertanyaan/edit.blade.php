@extends('layouts.master')

@section('content')
<div class="card card-primary">
  <div class="card-header">
    <h3 class="card-title">Form Pertanyaan</h3>
  </div>
  <!-- /.card-header -->
  <!-- form start -->
  <form role="form" action="{{ action('PertanyaanController@update', $pertanyaan[0]->id) }}" method="post">
    @csrf
    @method('PUT')
    <div class="card-body">
      <div class="form-group">
        <label for="judul">Judul</label>
        <input type="text" class="form-control @error('judul') is-invalid @enderror" name="judul" id="judul" value="{{ $pertanyaan[0]->judul }}" maxlength="45">
        @error('judul')
          <small class="text-danger">Judul harus diisi!</small>
        @enderror
      </div>
      <div class="form-group">
        <label for="isi">Pertanyaan</label>
        <textarea name="isi" id="isi" class="form-control @error('isi') is-invalid @enderror" placeholder="Pertanyaan (Maksimal 255 Karakter)" maxlength="255">{{ $pertanyaan[0]->isi }}</textarea>
        @error('isi')
          <small class="text-danger">Pertanyaan harus diisi!</small>
        @enderror
      </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Perbaharui</button>
    </div>
  </form>
</div>

@if ($errors->any())
  @push('scripts')
  <script>
    $(function () {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
      })    

      Toast.fire({
        icon: 'error',
        title: 'Pertanyaan gagal dibuat!'
      })
    })
  </script>
  @endpush
@endif
@endsection

@push('scripts')
  <script>
    let nav = document.querySelector('nav ul.nav')
    let navTreeview = nav.querySelectorAll('.has-treeview')

    navTreeview[0].classList.add('menu-open')
    navTreeview[0].children[0].classList.add('active')
    navTreeview[0].children[1].children[1].children[0].classList.add('active')
  </script>
@endpush