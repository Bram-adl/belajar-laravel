@extends('layouts.master')

@section('content')
<div class="card">
  <div class="card-header bg-primary">
    <h3 class="card-title">
      {{ $pertanyaan[0]->judul }}
    </h3>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <dl class="dl-horizontal">
      <dt>Pertanyaan :</dt>
      <dd>
        {{ $pertanyaan[0]->isi }}
      </dd>
      
      <dt>Tanggal Dibuat :</dt>
      <dd>
        {{ $pertanyaan[0]->tanggal_dibuat }}
      </dd>
      
      <dt>Tanggal Diperbaharui :</dt>
      <dd>
        {{ $pertanyaan[0]->tanggal_diperbaharui }}
      </dd>
    </dl>
    <a href="/pertanyaan" class="btn btn-primary">Kembali</a>
  </div>
  <!-- /.card-body -->
</div>
@endsection

@push('scripts')
<script>
  let nav = document.querySelector('nav ul.nav')
  let navTreeview = nav.querySelectorAll('.has-treeview')

  navTreeview[0].classList.add('menu-open')
  navTreeview[0].children[0].classList.add('active')
  navTreeview[0].children[1].children[1].children[0].classList.add('active')
</script>
@endpush